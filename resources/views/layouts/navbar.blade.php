<nav id="sidebar" class="sidebar">
	<div class="sidebar-content js-simplebar">
		<a class="sidebar-brand" href="index.html">
			<span class="align-middle">AdminKit</span>
		</a>

		<ul class="sidebar-nav">
			<li class="sidebar-header">
				Pages
			</li>
			<li class="sidebar-item active">
				<a class="sidebar-link" href="/">
					<i class="align-middle" data-feather="sliders"></i> <span class="align-middle">Dashboard</span>
				</a>
			</li>
			<li class="sidebar-item">
				<a class="sidebar-link" href="/tag">
					<i class="align-middle" data-feather="hash"></i> <span class="align-middle">Tag</span>
				</a>
			</li>
			<li class="sidebar-item">
				<a class="sidebar-link" href="pages-profile.html">
					<i class="align-middle" data-feather="user"></i> <span class="align-middle">Profile</span>
				</a>
			</li>
			<li class="sidebar-header">
				Tools & Components
			</li>
			<li class="sidebar-item">
				<a class="sidebar-link" href="pages-settings.html">
					<i class="align-middle" data-feather="settings"></i> <span class="align-middle">Settings</span>
				</a>
			</li>
		</ul>
		<div class="sidebar-cta">
			<div class="sidebar-cta-content">
				<strong class="d-inline-block mb-2">Kelompok 4</strong>
				<div class="mb-3 text-sm">
					Are you looking for more components? Check out our premium version.
				</div>
				<div class="d-grid">
					<a href="https://adminkit.io/pricing" target="_blank" class="btn btn-primary">Upgrade to Pro</a>
				</div>
			</div>
		</div>
	</div>
</nav>
<div class="main">
	<nav class="navbar navbar-expand navbar-light navbar-bg">
		<a class="sidebar-toggle d-flex">
			<i class="hamburger align-self-center"></i>
		</a>

		<form class="d-none d-sm-inline-block">
			<div class="input-group input-group-navbar">
				<input type="text" class="form-control" placeholder="Search…" aria-label="Search">
				<button class="btn" type="button">
					<i class="align-middle" data-feather="search"></i>
				</button>
			</div>
		</form>

		<div class="navbar-collapse collapse">
			<ul class="navbar-nav navbar-align">
				<li class="nav-item dropdown">
					<a class="nav-icon dropdown-toggle d-inline-block d-sm-none" href="#" data-bs-toggle="dropdown">
						<i class="align-middle" data-feather="settings"></i>
					</a>

					<a class="nav-link dropdown-toggle d-none d-sm-inline-block" href="#" data-bs-toggle="dropdown">
						<img src="{{asset('/template')}}/img/avatars/avatar.jpg" class="avatar img-fluid rounded me-1"
							alt="Charles Hall" /> <span class="text-dark">Charles Hall</span>
					</a>
					<div class="dropdown-menu dropdown-menu-end">
						<a class="dropdown-item" href="pages-profile.html"><i class="align-middle me-1"
								data-feather="user"></i> Profile</a>
					
						<div class="dropdown-divider"></div>
						<a class="dropdown-item" href="pages-settings.html"><i class="align-middle me-1"
								data-feather="settings"></i> Settings & Privacy</a>
						<a class="dropdown-item" href="/help"><i class="align-middle me-1" data-feather="help-circle"></i>
							Help Center</a>
						<div class="dropdown-divider"></div>
						<a class="dropdown-item" href="#">Log out</a>
					</div>
				</li>
			</ul>
		</div>
	</nav>